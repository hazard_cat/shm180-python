#ifndef __SHM180FORMAT__
#define __SHM180FORMAT__

#pragma pack(1)

/**
   Информация о расположении данных в файле
 */
struct Header {
  short revision;
  int info_offset;
  short info_length;
  int setup_offset;
  short setup_length;
  int data_block_offset;
  short no_of_data_blocks;
  int data_block_length;
  int meas_desc_block_offset;
  short no_of_meas_desc_blocks;
  short meas_desc_block_length;
  unsigned short header_valid; //0x5555 - valid, 0x1111 - not valid
  unsigned int reserved1;
  unsigned short reserved2; //Length of the data block extension header
  unsigned short chksum;
};

struct MesurementDescriptionStruct{
  char time[9];
  char date[11];
  short meas_mode;
  unsigned short points;
  unsigned short no_of_accum_curves;
  short first_curve;
  short curves;
  float col_time;
  float rep_time;
  short repeat;
  unsigned short cycles;
  float all_time;
  short use_motor;
  char reserved1;
  short reserved2;
};

struct DataBlock{
  short block_no;
  int data_offs;
  int next_block_offs;
  unsigned short block_type;
  short meas_desc_block_no;
  unsigned int reserved1;
  unsigned int reserved2;
};

struct DataBlockExt{
  char mod_ser_no[16];
  short trigger;
  float gate_level;
  float inp_thershold;
  unsigned short event_thershold;
  int event_no;
  float reserved2;
};

// В зависимости от значения поля meas_mode интрепретация данных будет отличаться
// в случае, если это multiscaler mode, то идут беззнаковые 32битные числа
// в случае event mode идут

struct Shm180File {
  struct Header headerBlock;
  struct MesurementDescriptionStruct *measDescBlocks;
  struct DataBlock *dataBlocks;
  struct DataBlockExt *dataExtBlocks;
  char *infoBlock;
  char *setupBlock;
  
};

void InitShm180File(struct Shm180File *data);
void FreeShm180File(struct Shm180File *data);
void ReadShm180File(const char* fname, struct Shm180File *data);

#endif
