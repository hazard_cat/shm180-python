"""
Описание структуры файла sdt
"""
import numpy as np


Header_t = [
    ('revision', 'i2'),
    ('info_offset', 'i4'),
    ('info_length', 'i2'),
    ('setup_offset', 'i4'),
    ('setup_length', 'i2'),
    ('datablock_offset', 'i4'),
    ('no_of_datablocks', 'i2'),
    ('datablock_length', 'i4'),
    ('measdescblock_offset', 'i4'),
    ('no_of_measdescblocks', 'i2'),
    ('measdescblosk_length', 'i2'),
    ('header_valid', 'u2'),
    ('reserved1', 'i4'),
    ('reserved2', 'i2'),
    ('chksum', 'u2')
]

MesurementDescription_t = [
    ('time', '|S9'),
    ('date', '|S11'),    
    ('meas_mode', 'i2'),
    ('points', 'u2'),
    ('no_of_accum_curves', 'u2'),
    ('first_curve', 'i2'),
    ('curves', 'i2'),
    ('col_time', 'f4'),
    ('rep_time', 'f4'),
    ('repeat', 'i2'),
    ('cycles', 'u2'),
    ('all_time', 'f4'),
    ('use_motor', 'i2'),
    ('reserved1', 'i1'),
    ('reserved2', 'i2')
]

DataBlock_t = [
    ('block_no', 'i2'),
    ('data_offs', 'i4'),
    ('next_block_offs', 'i4'),
    ('block_type', 'u2'),
    ('measdeckblock_no', 'i2'),
    ('reserved1', 'u4'),
    ('reserved2', 'u4')
] 

DataBlockExt_t = [
    ('mod_ser_no', 'S16'),
    ('trigger', 'i2'),
    ('gate_level', 'f4'),
    ('inp_threshold', 'f4'),
    ('event_threshold', 'u2'),
    ('events_no', 'i4'),
    ('reserved2', 'f4')
]

class Shm180File:
    
    def __init__(self, fname):
        """
        """
        
        with open(fname, 'rb') as f:
            self.header = np.fromfile(f, dtype=Header_t, count=1)
            f.seek(self.header['info_offset'])
            self.info = f.read(self.header['info_length']).decode('latin')
            
            f.seek(self.header['setup_offset'])
            self.setup = f.read(self.header['setup_length']).decode('latin')
            
            f.seek(self.header['measdescblock_offset'])
            self.measblock = np.fromfile(f, dtype=MesurementDescription_t, \
                count=self.header['no_of_measdescblocks'])
            print(self.measblock)
            f.seek(self.header['datablock_offset'])
            self.datablock = np.empty(self.header['no_of_datablocks'], dtype=DataBlock_t)
            print(self.header['reserved2'])
            print(self.header['datablock_length'])
            self.data = []
            
            for i in range(self.header['no_of_datablocks']):
                self.datablock[i] = np.fromfile(f, dtype=DataBlock_t, count=1)
                
                f.seek(self.datablock[i]["data_offs"])

                id = self.datablock[i]['measdeckblock_no']
                points = self.measblock[id]['points']
                
                self.data.append(np.fromfile(f, dtype='int32', count=points))
            
                f.seek(self.datablock[i]['next_block_offs'])

                
            
s = Shm180File('../a01.sdt')
s.data = np.array(s.data)
s.data=s.data.astype('int32')
import pylab as plt
plt.plot(s.data[0,:])
plt.show()