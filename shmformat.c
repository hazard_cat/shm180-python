#include "shmformat.h"
#include <stdio.h>
#include <stdlib.h>

void InitShm180File(struct Shm180File *data)
{
  printf("Initializing structure...\n");
  
  data->measDescBlocks = NULL;
  data->dataBlocks = NULL;
  data->dataExtBlocks = NULL;
  data->infoBlock = NULL;
  data->setupBlock = NULL;
}

/**
 * Освобождаем выделенную для хранения объектов память
 */
void FreeShm180File(struct Shm180File *data)
{
  printf("Release memory...\n");
  
  if(data->measDescBlocks)
    free(data->measDescBlocks);
  if(data->dataBlocks)
    free(data->dataBlocks);
  if(data->dataExtBlocks)
    free(data->dataExtBlocks);
  if(data->infoBlock)
    free(data->infoBlock);
  if(data->setupBlock)
    free(data->setupBlock);
}

/**
 * Читаем файл.
 * Все элементы структуры нам не важны, кроме самих данных
 */
void ReadShm180File(const char *fname, struct Shm180File *data)
{
  FILE *fin=NULL;

  // Читаем файл
  if((fin=fopen(fname, "rb"))!=NULL)
    {
      int count = fread(&data->headerBlock, sizeof(struct Header), 1, fin);
      printf("Have read %d objects\n", count);

      
      // get mem 
      fseek(fin, data->headerBlock.info_offset, SEEK_SET);
      data->infoBlock = (char*)calloc(data->headerBlock.info_length+1, sizeof(char));
      count = fread(data->infoBlock, sizeof(char), data->headerBlock.info_length, fin);

      printf("Have read %d objects\n", count);
     
      // get mem 
      fseek(fin, data->headerBlock.setup_offset, SEEK_SET);
      data->setupBlock = (char*)calloc(data->headerBlock.setup_length+1, sizeof(char));
      count = fread(data->setupBlock, sizeof(char), data->headerBlock.setup_length, fin);

      printf("Have read %d objects\n", count);

      // get mem
      fseek(fin, data->headerBlock.meas_desc_block_offset, SEEK_SET);
      data->measDescBlocks =
	(struct MesurementDescriptionStruct*)calloc(
						    data->headerBlock.meas_desc_block_length,
						    data->headerBlock.no_of_meas_desc_blocks
						    );
      count = fread(data->measDescBlocks, data->headerBlock.meas_desc_block_length,
		    data->headerBlock.no_of_meas_desc_blocks, fin);

      printf("Have read %d objects\n", count);
      
      fclose(fin);
    }
  else
    {
      printf("Error opening file <%s>\n", fname);
    }
  
}

